const chai = require('chai');
const expect = chai.expect;

const http = require('chai-http');
chai.use(http);

describe('api_test_suite_1', () => {
	it("test_api_get_people_is_running", () => {
		chai.request('http://localhost:5001').get('/people')
		.end((err,res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it("test_api_get_people_returns_200", (done) => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err,res) => {
			expect(res.status).to.equal(200)
			done()
		})
	})

	it("test_api_post_person_returns_400_no_person_name", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "James",
			age: 28
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})
	
	it("test_api_post_person_returns_400_if_age_is_string", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "James",
			age: "Twenty-Eight"
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})
})

describe('api_test_suite_2' , () => {
	it("test_api_post_person_returns_400_if_age_is_string", () => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "James",
			age: 28
		})
		.end((err,res) => {
			expect(res.status).to.not.equal(400)
		})
	})

	it("test_api_post_person_returns_400_no_name_field", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			age: 28
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})
	
	it("test_api_post_person_returns_400_no_age_field", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "James"
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})
	
})