const express = require('express');

const app = express();

const PORT = 5001;

app.use(express.json());

require('./app/routes.js')(app, {});

app.listen(PORT, () => {
	console.log('Running on port '+PORT);
})

//npx kill-port 5001
	// just in case na nagamit yung port
// ctrl + C para ma terminate
